#/usr/bin/env python3
from app import app

# Run Server
if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=False, threaded=False, use_reloader=False)
