from app import db
from app import COUNTRY
from datetime import datetime
from app.models.notification_model import NotificationPy, NotificationCo

def country_spesifics(COUNTRY):
    if(COUNTRY == 'paraguay'):
        return('')
    elif(COUNTRY == 'colombia'):
        return(''', p."process_url", p."tender_description"''')
    else:
        raise(ValueError(f"No such country is supported: {COUNTRY}"))


def get_notification_foolowUp_info():
    sql = f''' select n."status", n."event", n."tweet_id", n."parent_id", n."ocid", n."notification_date"
    from {COUNTRY}.notification n
    where "status" = 'PENDING' and tweet_id is not null
    order by n."id" asc
    limit 1;
    '''
    r = db.session.execute(sql)
    db.session.close()
    return(r) # pylint: disable=no-member


def get_notification_info():
    csp = country_spesifics(COUNTRY)
    sql = f'''WITH NALL AS (
        SELECT n."id", n."ocid", n."characteristics", n."rules",
            p."tender_title", p."buyer_name", n."tweet_id",
            n."parent_id", n."notification_date", p."tender_amount",
            p."tender_currency", n."status", p."tender_id"{csp}
        FROM {COUNTRY}.notification n
            JOIN {COUNTRY}.procurement p ON p."ocid" = n."ocid"
        WHERE "tweet_id" IS NULL
            AND "status" = 'PENDING'
    ),
    NBLL AS (
        SELECT "ocid" notocid, "tweet_id" notweet_id
        FROM {COUNTRY}.notification as a
        WHERE a."status" = 'NOTIFIED' OR a."tweet_id" IS NOT NULL
    )
    SELECT * FROM NALL
        LEFT JOIN NBLL ON NALL."ocid" = NBLL."notocid"
    AND NALL."ocid" IS NOT NULL
    AND NALL."rules" IS NOT NULL
    ORDER BY "id" ASC
    LIMIT 1;
    '''
    r = db.session.execute(sql) # pylint: disable=no-member
    db.session.close()
    return(r)


def update_notification(ocid, tw_id: str = None) -> None:
    if(COUNTRY == 'paraguay'):
        noti = NotificationPy.query.filter_by(ocid=ocid).first()
    elif(COUNTRY == 'colombia'):
        noti = NotificationCo.query.filter_by(ocid=ocid).first()
    else:
        raise(ValueError(f"No such country is supported: {COUNTRY}"))
    
    noti.status = 'NOTIFIED'
    noti.tweet_id = tw_id
    noti.notification_date = datetime.now().astimezone()
    db.session.commit() # pylint: disable=no-member
    db.session.close()
