# Database

# Docker values
DATABASE_URL = "51.158.117.91:19432"
DATABASE_USER = "empatia"
DATABASE_PW = "5433mp@t1@123"
DATABASE_DB = "postgresql"
DATABASE_DRIVER = "postgresql"
DATABASE_NAME = "empatia"

# local values
#DATABASE_URL = "127.0.0.1:5432"
#DATABASE_USER = "postgres"
#DATABASE_PW = "postgres"
#DATABASE_DB = "postgresql"
#DATABASE_DRIVER = "postgresql"
#DATABASE_NAME = "empatia"

DB_URI = f'{DATABASE_DB}://{DATABASE_USER}:{DATABASE_PW}@{DATABASE_URL}/{DATABASE_NAME}'
