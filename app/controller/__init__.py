from app import app
from flask import jsonify, request, Blueprint
from flask import session
from functools import wraps
import jwt
import datetime
from werkzeug.security import check_password_hash

api = Blueprint('api', __name__)


def check_for_token(func):
    @wraps(func)
    def wrapped(*args, **kwargs):
        # Remove Bearer from token
        token = request.headers.get('Authorization')
        if not token:
            return(jsonify({'message': 'Missing token'}), 403)
        try:
            token_check = token.split()[1]
            # data = jwt.decode(token_check, app.config['SECRET_KEY'])
            jwt.decode(token_check, app.config['SECRET_KEY'])
        except Exception:
            return(jsonify({'message': 'Invalid token'}), 403)
        return func(*args, **kwargs)
    return(wrapped)


# other endpoints
# from .user_endpoint import get_users, get_user, create_user
# from .bot_controller import tweet_something
# from .notification_controller import get_notifications
