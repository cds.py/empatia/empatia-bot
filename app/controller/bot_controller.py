''' from app.controller import api
from flask import request, jsonify
from app.services.bot_service import bot_tweet_something, bot_reply_to_tweet

# DEBUG FUNCTIONS
@api.route('/test-tweet', methods=['POST'])
def tweet_something():
    data = request.get_json()
    try:
        info = data['info']
        return(bot_tweet_something(info))
    except KeyError as err:
        print(err)
        return(jsonify({'status': 'error', 'error': str(err)}))


@api.route('/test-tweet/reply', methods=['POST'])
def reply_something():
    data = request.get_json()
    try:
        message = data['message']
        parent_id = data['parent_id']
        return(bot_reply_to_tweet(message, parent_id))
    except KeyError as err:
        print(err)
        return(jsonify({'status': 'error', 'error': str(err)}))'''
