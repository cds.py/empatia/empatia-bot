""" from flask import jsonify
from app.controller import api
from app.services.notification_service import notification_get, notification_get_updates, notification_send, notification_send_updates
import traceback

# DEBUG FUNCTIONS
@api.route('/notifications')
def get_notifications():
    try:
        notification_dicts, notification, notification_out = [], {}, []
        notification_dicts = notification_get()

        if(not(notification_dicts)):
            return(jsonify({'status': 'None',
                            'None': 'No notification pending'}))

        for notification_dict in notification_dicts:
            for KEY, VALUE in notification_dict.items():
                notification[KEY] = str(VALUE)
            notification_out.append(notification)

        return(jsonify(str(notification)))

    except Exception as err:
        return(jsonify({'status': 'ERROR',
                        'error': str(err),
                        'trace': str(traceback.format_exc()),
                        'where': f'''notification_get():{str(type(notification_dicts))} ==> {str(notification_dicts)}'''}))


@api.route('/notifications/updates')
def get_notification_updates():
    try:
        notification_dicts, notification, notification_out = [], {}, []
        notification_dicts = notification_get_updates()

        if(not(notification_dicts)):
            return(jsonify({'status': 'None',
                            'None': 'No notification pending'}))

        for notification_dict in notification_dicts:
            for KEY, VALUE in notification_dict.items():
                notification[KEY] = str(VALUE)
            notification_out.append(notification)

        return(jsonify(str(notification)))

    except Exception as err:
        return(jsonify({'status': 'ERROR',
                        'error': str(err),
                        'trace': str(traceback.format_exc()),
                        'where': f'''notification_get_updates():{str(type(notification_dicts))} ==> {str(notification_dicts)}'''}))


@api.route('/notifications/all')
def get_all_notifications():
    try:
        notifications = notification_send()

        if(notifications):
            return(jsonify({
                'status': 'success',
                'data': str(notifications)}))
        else:
            return(jsonify({'status': 'None',
                            'None': 'No notification pending'}))

    except Exception as err:
        return(jsonify({'status': 'ERROR',
                        'error': str(err),
                        'trace': str(traceback.format_exc()),
                        'where': f'''notification_send():{str(type(notifications))} ==> {str(notifications)}'''}))

@api.route('/notifications/do-notify-now')
def do_notify_now():
    'debuging tool'
    try:
        o = [notification_send()]
    except Exception as err_:
        o.append(f'''notification_send():{str(type(err_))} ==> {str(err_)}''')
        o.append(traceback.format_exc())
    finally:
        return(jsonify(str(o)))


@api.route('/notifications/do-update-now')
def do_update_now():
    'debuging tool'
    try:
        o = [notification_send_updates()]
    except Exception as err_:
        o.append(f'''notification_send():{str(type(err_))} ==> {str(err_)}''')
        o.append(traceback.format_exc())
    finally:
        return(jsonify(str(o)))

@api.route('/notifications/do-all-now')
def do_all_now():
    'Debugging tool'
    try:
        o = [notification_send(), notification_send_updates()]
    except Exception as err_:
        o.append(f'''notification_send():{str(type(err_))} ==> {str(err_)}''')
        o.append(traceback.format_exc())
    finally:
        return(jsonify(str(o)))
"""
