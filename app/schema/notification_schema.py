from app import ma


class NotificationSchemaCo(ma.Schema):
    class Meta:
        fields = ('id', 'ocid', 'characteristics', 'tender_title', 'rules', 'tender_description', 'buyer_name', 'tender_amount', 'tender_currency', 'status', 'tender_id', 'process_url')

class NotificationSchemaPy(ma.Schema):
    class Meta:
        fields = ('id', 'ocid', 'characteristics', 'tender_title', 'rules', 'buyer_name', 'tender_amount', 'tender_currency', 'status', 'tender_id', 'process_url')

class NotificationUpdateSchema(ma.Schema):
    class Meta:
        fields = ('id', 'ocid', 'characteristics', 'status', 'rules', 'event', 'tweet_id', 'parent_id', 'creation_date', 'notification_date')


#notification_schema = NotificationSchema()

notification_paraguay_list_schema = NotificationSchemaPy(many=True)
notification_colombia_list_schema = NotificationSchemaCo(many=True)

#notification_update_schema = NotificationUpdateSchema()
notification_update_list_schema = NotificationUpdateSchema(many=True)
