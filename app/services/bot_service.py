from tweepy.error import TweepError
from app.repository.notification_repository import update_notification
from app import COUNTRY
from app import tw_api
# import uuid
from tweepy import TweepError
from typing import Tuple, Union

def bot_tweet_something(message) -> dict:
#     return(bot_fake_tweet(message))
    try:
        status = tw_api.update_status(message)
        return {'status': 'success', 'tw_id': str(status._json['id']), "info": status}
    except TweepError as err:
        if (err.api_code == 187):
            # Status is duplicated
            print(f'[{COUNTRY}] Status is duplicated (was the bot rebooted mid tweet?)')
            return({'status': 'error', 'error': 'duplicated', 'code': 187})
    except Exception as err:
        print(err)
        return({'status': 'error', 'error': str(err)})


def bot_reply_to_tweet(message, parent_tweet_id) -> dict:
#    return(bot_fake_reply_to_tweet(message, parent_tweet_id))
    try:
        status = tw_api.update_status(
            status=message,
            in_reply_to_status_id=int(parent_tweet_id)
        )
        return({'status': 'success', 'tw_id': str(status._json['id'])})
    except Exception as err_:
        print(err_)
        return({'status': 'error', 'error': str(err_)})

def fix_duplicated_status(ocid: str) -> Tuple[bool, Union[str, int]]:
    last_status = tw_api.user_timeline(id = tw_api.me()._json['id'], count = 1)[0]
    if (last_status.in_reply_to_status_id is None):
        tw_id = last_status.id
        print(f'[{COUNTRY}] Using last tweet as tweet_id')
        update_notification(ocid, tw_id)
        return(True, tw_id)
    else:
        tw_id = last_status.in_reply_to_status_id
        print(f'[{COUNTRY}] Using last parent tweet as tweet_id')
        update_notification(ocid, tw_id)
        return(False, None)

#def bot_fake_tweet(message):
#    try:
#        print(f'Tweeted: \n{message}\nLenght: {len(message)}')
#        if len(message) > 279:
#            print('WARNING: TWEET IS TOO LONG!!!!11!1!!one')
#            raise(Exception('Tweet too long'))
#        return({
#            'status': 'success',
#            'tw_id': str(uuid.uuid1().int >> 64)
#        })
#    except Exception as err_:
#        return({'status': 'error', 'error': str(err_)})

# def bot_fake_reply_to_tweet(message, parent_tweet_id):
#     print(f'In response to: {str(parent_tweet_id)}')
#     status = bot_fake_tweet(message)
#     try:
#         return({'status': 'success', 'tw_id': str(status['tw_id'])})
#     except KeyError:
#         return(status)