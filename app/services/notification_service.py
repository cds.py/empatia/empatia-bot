from app.repository.notification_repository import get_notification_info, update_notification, get_notification_foolowUp_info
from app.schema.notification_schema import notification_colombia_list_schema, notification_paraguay_list_schema, notification_update_list_schema  # , notification_schema
from app.services.bot_service import bot_tweet_something, bot_reply_to_tweet, fix_duplicated_status
from app import COUNTRY
from typing import List, Union
import time
import requests
import urllib
import traceback
from tweepy.error import TweepError
from apscheduler.schedulers.background import BackgroundScheduler
scheduler = BackgroundScheduler()
scheduler.start()

TINY_URL = "http://tinyurl.com/api-create.php"
DNCP_URL = "https://www.contrataciones.gov.py/licitaciones/convocatoria/"
DNCP_URL_SEARCH = "https://www.contrataciones.gov.py/buscador/licitaciones.html"


def notification_get() -> List[dict]:
    if(COUNTRY == 'paraguay'):
        return(notification_paraguay_list_schema.dump(get_notification_info()))
    elif(COUNTRY == 'colombia'):
        return(notification_colombia_list_schema.dump(get_notification_info()))
    else:
        raise(ValueError(f"No such country is supported: {COUNTRY}"))


def notification_get_updates() -> List[dict]:
    return(notification_update_list_schema.dump(get_notification_foolowUp_info()))


def notification_control():
    start_cron()


def get_cut_ocid(notif: dict) -> str:
    if(COUNTRY == 'paraguay'):
        return(notif['ocid'].split('-')[2])
    elif(COUNTRY == 'colombia'):
        return(notif['tender_id'])
    else:
        raise(ValueError(f"No such country is supported: {COUNTRY}"))

def notification_send() -> list:
    print(f'[{COUNTRY}] Sending  Notification: {print_date_time()}')
    list_notifications, list_msgs = notification_get(), []

    if not(bool(list_notifications)):
        print(f'[{COUNTRY}] -> Nothing to do.')
        return([])  # If no notifications avalible, return cleanly

    notif = list_notifications[0]
    try:
        cut_ocid = get_cut_ocid(notif)

        print(f'[{COUNTRY}] -> On: {COUNTRY}: {notif["ocid"]}')

        if(COUNTRY == 'paraguay'):
            tiny_url = get_bid_url_from_TenderID(notif['tender_id'], cut_ocid)
        elif(COUNTRY == 'colombia'):
            tiny_url = get_bid_url_from_ProcessURL(notif['process_url'])
        else:
            raise(ValueError(f"No such country is supported: {COUNTRY}"))
    
        if (tiny_url is None) or (tiny_url == ''):
            print(f"[{COUNTRY}][{cut_ocid}] could not get tinyurl.")
        else:
            print(f"[{COUNTRY}][{cut_ocid}] got tinyurl: [{tiny_url}]")

        list_chars = ''
        if(isinstance(notif['characteristics'], str)):
            for charsc in notif['characteristics'].split(' '):
                list_chars = list_chars + '#' + charsc.replace('_', '') + ' '
            list_chars = list_chars[:-1]
            
        elif(isinstance(notif['characteristics'], list)): # pylint: disable=not-an-iterable
            for charsc in notif['characteristics']: # pylint: disable=not-an-iterable
                list_chars = list_chars + '#' + charsc.replace('_', '') + ' '
            list_chars = list_chars[:-1]
            
        else:
            list_chars = ''

        if (notif.get('tender_currency') == 'PYG'):
            curr = 'Gs.'
        elif(notif.get('tender_currency') == 'COP'):
            curr = 'COP$'
        else:
            curr = 'USD'

        if(bool(notif['tender_amount'])):
            message = 'Compra '

            if(bool(notif.get('buyer_name'))):
                message += 'de ' + notif['buyer_name'] + ' '
                
            if not(notif.get('tender_amount') is None):
                message += f'por {curr} '
                message += (str("{:,.0f}".format(notif['tender_amount'])).replace(',', '.') + ' ' + list_chars).rstrip()
            else:
                print(Exception("Tender ammount is NULL"))
                update_notification(notif['ocid'])
                return([])
        else:
            update_notification(notif['ocid'])
            return([])

        if bool(tiny_url):
            message = (message + '\nEnlace: ' + tiny_url)


        tweet_info = bot_tweet_something(message)
        list_msgs.append(message)

        if (tweet_info['status'] == 'success'):
            tw_id = tweet_info['tw_id']
            print(f'[{COUNTRY}] --> Tweeted({str(tw_id)}): {str(message)}')
            update_notification(notif['ocid'], tw_id)
            list_msgs.append(followup_notification(cut_ocid, notif, tw_id))
        elif(tweet_info['code'] == 187):
            isbase, tw_id = fix_duplicated_status(ocid=notif['ocid'])
            if isbase:
                list_msgs.append(followup_notification(cut_ocid, notif, tw_id))
            return(list_msgs)
        else:
            raise(Exception(tweet_info))
    except Exception as err_:
        print(f'ON: {COUNTRY}')
        print(err_)
        print(traceback.format_exc())
    finally:
        return(list_msgs)

def followup_notification(cut_ocid: str, notif: dict, tw_id: Union[str, int]) -> str:
    if(COUNTRY == 'paraguay'):
        msg_response = 'Convocatoria: ' + cut_ocid + ' - ' + ' '.join(notif['tender_title'][:(261 - (len(notif['tender_title'] + cut_ocid)))].split(' ')).rstrip(' ') + '…'
    elif(COUNTRY == 'colombia'):
        msg_response = 'Convocatoria: ' + cut_ocid + ' - ' + ' '.join(notif['tender_description'][:(261 - (len(notif['tender_description'] + cut_ocid)))].split(' ')).rstrip(' ') + '…'
    else:
        raise(ValueError(f"No such country is supported: {COUNTRY}"))
    
    tweet_response = bot_reply_to_tweet(msg_response, tw_id)
    print(f'[{COUNTRY}] Tweeted({str(tweet_response["tw_id"])}): {str(msg_response)}')
    
    if (tweet_response['status'] == 'success'):
        if not(notif['rules'] is None):
            rule_response = bot_reply_to_tweet( # pylint: disable=unused-variable
                message='Clasificación Atípica: ' + ('Baja. ' if notif['rules']['risk'] == "yellow" else 'Alta. ') + ((notif['rules']['rules'][:249].rstrip(' ') + '…') if (len(notif['rules']['rules']) > 249) else notif['rules']['rules']),
                parent_tweet_id=tweet_response['tw_id'],
            )
        elif(COUNTRY == 'colombia'):
            pass # Nothing to do here, move along
        else:
            print(f'[{COUNTRY}][{notif["ocid"]}] NO RULES')
    else:
        raise(Exception(tweet_response))
    
    return(msg_response)

def notification_send_updates():
    print(f'[{COUNTRY}] Updating Notification: {str(print_date_time())}')
    try:
        notifications_updates_list, o = notification_get_updates(), []
        if(not(notifications_updates_list)):
            print(f'[{COUNTRY}] -> Nothing to do.')
            return({})
        for notif in notifications_updates_list:
            if(notif is None): continue
            tweet_info = bot_reply_to_tweet(
                notif['event'],
                notif['tweet_id']
            )
            print(f'[{COUNTRY}] -> On: {COUNTRY}: {notif["ocid"]}')
            if (tweet_info['status'] == 'success'):
                print(f'[{COUNTRY}] --> Tweeted({str(tweet_info["tw_id"])}): {str(notif["event"])}')
                update_notification(ocid=notif['ocid'], tw_id=tweet_info['tw_id'])
            else:
                print(TweepError(tweet_info['error']))
                print(traceback.format_exc())
            o.append(notif['event'])
        return(str(o))
    except Exception as err_:
        print(err_)
        print(traceback.format_exc())


def start_cron():
    minutes: int = 10

    print(f'[{COUNTRY}][SCHEDULER] Running every: {str(minutes)} minutes.')
    scheduler.add_job(notification_send, name='notification-send', max_instances=1, trigger="interval", minutes=minutes)
    notification_send()

    print(f'[{COUNTRY}][SCHEDULER] Running every: {str(minutes)} minutes.')
    scheduler.add_job(notification_send_updates, name='notification-send-updates', max_instances=1, trigger="interval", minutes=minutes)
    notification_send_updates()


def stop_cron():
    scheduler.shutdown()
    # scheduler.remove_job(str(process_id))


def print_date_time():
    return(time.strftime("%A, %d. %B %Y %I:%M:%S %p"))


def get_bid_url_from_ProcessURL(process_url):
    try:
        if not(bool(process_url)):
            return('')
        res_full = requests.get(process_url.split('&')[0])
        res_full.raise_for_status()
        req_url = TINY_URL + '?' + urllib.parse.urlencode({'url': process_url})
        res = requests.get(req_url)
        res.raise_for_status()
        return(res.text)
    except Exception as err_:
        print(err_)


def get_bid_url_from_TenderID(tender_id, ocid):
    full_url = ''
    tiny_url = ''
    try:
        bid_url = DNCP_URL + tender_id + '.html'
        res_full = requests.get(bid_url)
        if (res_full.status_code == 200):
            full_url = bid_url
        else:
            full_url = DNCP_URL_SEARCH + '?nro_nombre_licitacion=' + ocid
        req_url = TINY_URL + '?' + urllib.parse.urlencode({'url': full_url})
        res = requests.get(req_url)
        if (res.status_code == 200):
            tiny_url = res.text
        return tiny_url
    except Exception as err_:
        print(err_)


def get_buyer(buyer_full):
    try:
        buyer_short = ''
        if (('(' in buyer_full) and (')' in buyer_full)):
            index_start = buyer_full.index('(')
            index_end = buyer_full.index(')')
            buyer_short = buyer_full[index_start + 1:index_end]
            return(buyer_short)
        else:
            return(buyer_full)
    except Exception as err_:
        print(err_)
        return(buyer_full)
