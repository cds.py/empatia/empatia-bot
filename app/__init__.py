from flask import Flask
from flask import current_app
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from app.repository import DB_URI
from flask_cors import CORS
from flask_mail import Mail
from flask_swagger_ui import get_swaggerui_blueprint
from flask_migrate import Migrate
import os
import logging
import tweepy

# Init App
app = Flask(__name__)
CORS(app)

# secret key
app.config['SECRET_KEY'] = 'empatia-bot'

# config db
app.config['SQLALCHEMY_DATABASE_URI'] = DB_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Init db
db = SQLAlchemy(app)

# Migrate
migrate = Migrate(app, db)

# Esta parte, de crear las tablas tengo duda porque no se si esta correcto de esta manera
""" from app.model import create_db
create_db()
db.create_all()
 """
# Schema
ma = Marshmallow(app)

# Authenticate to Twitter
CONSUMER_KEY = os.getenv("CONSUMER_KEY")
CONSUMER_SECRET = os.getenv("CONSUMER_SECRET")
ACCESS_TOKEN = os.getenv("ACCESS_TOKEN")
ACCESS_TOKEN_SECRET = os.getenv("ACCESS_TOKEN_SECRET")
COUNTRY = os.getenv("COUNTRY")

''' # PARAGUAY ACCOUNT
CONSUMER_KEY = "zBXWhT0wxh94ubXAbV4UfigoR"
CONSUMER_SECRET = "2ww1suZsv8F8dvjXx45LeGF6d3RV0rEFNt7VepoFcXOzaYriCC"
ACCESS_TOKEN = "1319725219590901760-ebGzw8jXq82dimIqxXhjLVIBpSlAxj"
ACCESS_TOKEN_SECRET = "tlII2AC0LH91ASLjrQ2hz1rUMNsTM50DAHavKAB2rOrcX"
COUNTRY = 'paraguay'
'''

''' # COLOMBIA ACCOUNT
CONSUMER_KEY = 'W3Q7v3G2mskUjeveCjW0liMBj'
CONSUMER_SECRET = 'NBRoGziJ0O6DQy0JCALwdK1YSTpO0U9MF0oBc9VYX5Sql5Yg3f'
ACCESS_TOKEN = '1323592356214513664-qrvaUfQtzx40dRKOUzibWyiw3rTFmw'
ACCESS_TOKEN_SECRET = 'XhvaZk0O1dxsuDnz96lbXjEnNIMiFwAnhpEMrmdBLpnLj'
COUNTRY = 'colombia'
'''


''' # TEST ACCOUNT
CONSUMER_KEY = 'DxTgxYcyJC5S6fsh6HXmUo6IK'
CONSUMER_SECRET = 'PFdckkh4nOsoM3re97iW1mbfTHwQY2zmOLEYszfwdQHn4W9cdH'
ACCESS_TOKEN = '1316098111358619648-Ne36QPq5MkzU0UPK2a2Ilt3f9lWwhd'
ACCESS_TOKEN_SECRET = '9y066iILLWLfOoqVcXe1FmChW4nXoduFSa1M72dWAIjd9'
COUNTRY = ['paraguay', 'colombia'][0]
'''

print(f'[{COUNTRY}][DATABASE]: {DB_URI}')

tw_auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
tw_auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

tw_api = tweepy.API(tw_auth)

try:
    tw_api.verify_credentials()
    print(f"[{COUNTRY}][Twitter] authentication: OK")
except Exception as err_:
    print(err_)
    print(f"[{COUNTRY}][Twitter] Error during authentication")

print(f"[{COUNTRY}][Twitter] Account ID: {str(tw_api.me()._json['id'])}")
print(f"[{COUNTRY}][Twitter] Username:   {tw_api.me()._json['name']}")

# Imports endpoints
# from app.controller import api
# app.register_blueprint(api, url_prefix='/empatia/api')

from app.services.notification_service import notification_control
notification_control()

# set logger
@app.before_first_request
def before_first_request():
    log_level = logging.INFO
    app.logger.setLevel(log_level)
    # print(app.url_map)
