from app import db


class NotificationPy(db.Model):

    """Notification model for Paraguay"""
    __tablename__ = "notification"

    id = db.Column(db.BigInteger, primary_key=True)
    ocid = db.Column(db.String(), nullable=False)
    characteristics = db.Column(db.JSON, nullable=False)
    status = db.Column(db.String(), nullable=True)
    rules = db.Column(db.JSON, nullable=True)
    event = db.Column(db.String(), nullable=True)
    tweet_id = db.Column(db.String(), nullable=True)
    parent_id = db.Column(db.BigInteger, nullable=True)
    creation_date = db.Column(db.DateTime, nullable=True)
    notification_date = db.Column(db.DateTime, nullable=True)
    rules = db.Column(db.JSON, nullable=True)

    __table_args__ = {'schema': 'paraguay'}


class NotificationCo(db.Model):

    """Notification model for Colombia"""
    __tablename__ = "notification"

    id = db.Column(db.BigInteger, primary_key=True)
    ocid = db.Column(db.String(), nullable=False)
    characteristics = db.Column(db.JSON, nullable=False)
    status = db.Column(db.String(), nullable=True)
    rules = db.Column(db.JSON, nullable=True)
    event = db.Column(db.String(), nullable=True)
    tweet_id = db.Column(db.String(), nullable=True)
    parent_id = db.Column(db.BigInteger, nullable=True)
    creation_date = db.Column(db.DateTime, nullable=True)
    notification_date = db.Column(db.DateTime, nullable=True)
    rules = db.Column(db.JSON, nullable=True)

    __table_args__ = {'schema': 'colombia'}
