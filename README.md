# empatia-bot

Bot de Twitter

## Requerimientos

- Python 3 y/o docker-compose
- Pip
- virtualenv

## Levantar localmente con docker

- `$ docker build . --tag cds/empatia-bot:1.0`
- `$ docker run -it --rm cds/empatia-bot`

Esta app está diseñada para trabajar en conjunto con [La ETL de empatia](https://gitlab.com/cds.py/empatia/empatia-etl)

La app se conecta a una base de datos postgresql que debe ser configurada en [este archivo](app/repository/__init__.py).

## Desarrollo

Para agregar soporte a nuevos países \(asumiendo compatibilidad con la ETL\) se deben agregar entradas en:

- [notification_repository.py](app/repository/notification_repository.py) en `country_spesifics()` y
 `update_notification()`
- [notification_schema.py](app/schema/notification_schema.py) añadir un `NotificationSchema` apropiado para el país e inicializarlo.
- [notification_service.py](app/services/notification_service.py) en `notification_send()` y `notification_get()` archivos.
- [app/\{NEW_COUNTRY\}rc](app/) deve [exportar](http://linuxcommand.org/lc3_man_pages/exporth.html) las credenciales de twitter de la cuenta y app.
- [entrypoint.sh](entrypoint.sh) deve ser modificado acorde a los nuevos `app/{NEW_COUNTRY}rc`

El bot está configurado para enviar tweets nuevos y enviar updates cada 10 minutos, esto puede ser modificado en [este archivo](app/services/notification_service.py) en la funcion `start_cron()`
